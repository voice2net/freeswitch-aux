#
# Script: create addBlocked.sh
#
#!/bin/bash
line1="        <condition field=\"caller_id_number\" expression=\"^($1)$|^1($1)$\" break=\"on-true\">"
line2="            <action application=\"answer\"/>"
line3="            <action application=\"sleep\" data=\"500\"/>"
line4="            <action application=\"playback\" data=\"/usr/local/freeswitch/sounds/en/us/callie/zrtp/16000/zrtp-thankyou_goodbye.wav\"/>"
line5="        </condition>"
alllines=''$line1'\n'$line2'\n'$line3'\n'$line4'\n'$line5
sed '/NO/ i\'"$alllines"'' $2 > $2.tmp
mv -f $2.tmp $2

