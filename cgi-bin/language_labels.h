/** language labels for v2n pbx2net */

char hostCompany[] = "Voice2Net Corp"; 
char* langDomainEmail[]={"The Domain Email","Le domaine email"};
char* langDomainUserPasswd[]={"Domain User Password","Mot de passe d'utilisateur de domaine"};
char* langDomainWebtimeout[]={"Web Access Timeout","Délai d'accès au Web"};