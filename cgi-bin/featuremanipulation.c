/* ************************************************************************************************************ */
#include <stdio.h>
#include <stdlib.h>
#include  <string.h>
#include <dirent.h>
#include <time.h>
#include <sys/stat.h>

void getDialPlanParameters();
void callForwardExternal();
void callForwardInternal();
void cancelCallForwarding();

void cancelDND();
void enableDND();
  char	  fscliPassword[24];
int       updateXmlFiles();
char theFile[128];
FILE *xfptr;
char theDndFile[256];

struct theExtension
{
    char cellPhone[56];
    char CallTimeout[12];
    char dialPlan[56];
    char extension[24];
    char codeDialed[12];
    char digitsDialed[24];
    char domain[128];
    char clid[12];  
    char dndStatus[6];  
    char voicemailEscape[24];
    char dndCfwdBypass[12];
}ext;//          //1=*code  2=digits dialed 3 = domain  4 = clid 5 = dialPlan
/* ************************************************************************************************************ 
arguments will be featurecode, digits_dialed.

*/


/* ******************************************************************************************************************************************* */
/* ******************************************************************************************************************************************* */
int main(int argc, char *argv[])
{

  FILE *fptr;
  char theSedFile[256];

  char theGatewayFile[256];
  char theSedCommand[256];
  char theData[128];
  char theGateway[24];

  setuid( 0 );
  
    char findPassword[256];
    char passwordData[128];
    char *adest,*bdest;
    
    strcpy(ext.digitsDialed,argv[2]);
    strcpy(ext.codeDialed,argv[1]);
    strcpy(ext.domain,argv[3]);
    strcpy(ext.clid,argv[4]);
    strcpy(ext.dialPlan,argv[5]);
    
    sprintf(findPassword,"grep \"param name=\\\"password\\\" value\" /usr/local/freeswitch/conf/autoload_configs/event_socket.conf.xml");
    FILE *ptr = popen(findPassword, "r");
    if(ptr)
    {
            fgets(passwordData,127,ptr);
            pclose(ptr);
            adest = strstr(passwordData,"value");
            if(adest)
            {
                     bdest = strchr(&passwordData[adest-passwordData+7],'"');
                     if(bdest)
                     {
                              strncpy(fscliPassword,&passwordData[adest-passwordData+7],bdest-adest-7);
                              fscliPassword[bdest-adest-7]=0;
                     }
            }
    }   

  xfptr = fopen("/tmp/feature.xml","a+");
  if(xfptr)
  {
          fprintf(xfptr,"0-{%s} 1-{%s} 2-{%s} 3-{%s} 4-{%s} 5-{%s}\n",argv[0],argv[1],argv[2],argv[3],argv[4],argv[5]);
          //1=*code  2=digits dialed 3 = domain  4 = clid 5 = dialPlan
          fclose(xfptr);
  }
  
  if(strstr(argv[1],"*72"))
  {//1=*72  2=cfwd_num  3=domain  4 = clid 5 = ${toll_allow}

        sprintf(theFile,"/usr/local/freeswitch/conf/dialplan/%s/%sdpn.xml",ext.domain,ext.clid);
        getDialPlanParameters();

        if(strlen(argv[2]) > 6)                 
        {
              callForwardExternal();                           
        }
        else
        {
             callForwardInternal();            
        }
  
  }
  else if(strstr(argv[1],"*73"))
  {
        sprintf(theFile,"/usr/local/freeswitch/conf/dialplan/%s/%sdpn.xml",ext.domain,ext.clid);       
       xfptr = fopen("/tmp/feature.xml","a+");
      if(xfptr)
      {
              fprintf(xfptr,"trying to cancel call forwading\n");
              fclose(xfptr);
      }
       
        getDialPlanParameters();
        xfptr = fopen("/tmp/feature.xml","a+");
        if(xfptr)
        {
                fprintf(xfptr,"got  call forwarding dial parameters toll allow {%s}\n",ext.domain);          
                fclose(xfptr);
        }

        sprintf(theFile,"/usr/local/freeswitch/conf/dialplan/%s/%sdpn.xml",ext.domain,ext.clid);
       cancelCallForwarding();
       xfptr = fopen("/tmp/feature.xml","a+");
       if(xfptr)
       {
              fprintf(xfptr,"Cancelled call forwarding\n");
    
              fclose(xfptr);
       }

  }  
 
  else if(strstr(argv[1],"*78"))
  {//do not disturb on setting the value to -1 will force the call to bypass,ie, DND  s    

        strcpy(ext.dndStatus,argv[6]);
        sprintf(theDndFile,"/usr/local/freeswitch/conf/directory/%s/%sreg.xml",argv[3],argv[4]);
        sprintf(theFile,"/usr/local/freeswitch/conf/dialplan/%s/%sdpn.xml",argv[3],argv[4]);          
        getDialPlanParameters(); 
        xfptr = fopen("/tmp/feature.xml","a+");
       if(xfptr)
       {
              fprintf(xfptr,"get parameters from {%s}\n",theFile);
    
              fclose(xfptr);
       }
        if(!strcmp(argv[6],"ON"))
        {
             cancelDND();                        
        }
        else
        {
            enableDND();
        }
  }
  updateXmlFiles();
  return 0;      
}
/* ******************************************************************************************************************************************* */
void getDialPlanParameters()
{
     char theData[1024];
     char *adest,*bdest,*cdest;
     FILE *fptr=fopen(theFile,"r");
     if(fptr)
     {
       while(!feof(fptr))
       {
             fgets(theData,256,fptr);
             if(strstr(theData,"call_timeout="))
             {
                 adest = strstr(theData,"call_timeout=");
                 if(adest)
                 {
                     bdest = strstr(theData,"\"/>");
                     if(bdest)
                     {
                         strncpy(ext.CallTimeout,&theData[adest-theData+13],bdest-adest-13);
                         ext.CallTimeout[bdest-adest-13]=0;
                     }
                 }                                           
             }
             else if(strstr(theData,"toll_allow="))
             {
                 adest = strstr(theData,"toll_allow=");
                 if(adest)
                 {
                     bdest = strstr(theData,"\"/>");
                     if(bdest)
                     {
                         strncpy(ext.dialPlan,&theData[adest-theData+11],bdest-adest-11);
                         ext.dialPlan[bdest-adest-11]=0;
                     }
                 }                           
             }
             else if(strstr(theData,"cellPhone="))
             {
                 adest = strstr(theData,"cellPhone=");
                 if(adest)
                 {
                     bdest = strstr(theData,"\"/>");
                     if(bdest)
                     {
                         strncpy(ext.cellPhone,&theData[adest-theData+10],bdest-adest-10);
                         ext.cellPhone[bdest-adest-10]=0;
                     }
                 }                           
             }
             else if(strstr(theData,"vm_zero_xfer="))
             {
                 adest = strstr(theData,"vm_zero_xfer=");
                 if(adest)
                 {
                     bdest = strstr(theData,"\"/>");
                     if(bdest)
                     {
                         strncpy(ext.voicemailEscape,&theData[adest-theData+13],bdest-adest-13);
                         ext.voicemailEscape[bdest-adest-13]=0;
                     }
                 }                           
             }         
             else if(strstr(theData,"caller_id_number"))
             {
                 adest = strstr(theData,"caller_id_number");
                 if(adest)
                 {
                     bdest = strstr(&theData[adest-theData],")$");
                     cdest = strstr(&theData[adest-theData],"^(");
                     if(bdest && cdest && bdest-cdest < 6)
                     {
                         strncpy(ext.dndCfwdBypass,&theData[cdest-theData+2],bdest-cdest-2);
                         ext.dndCfwdBypass[bdest-cdest-2] = 0;
                     }
                     else 
                         ext.dndCfwdBypass[0] = 0;
                 }
                 else
                         ext.dndCfwdBypass[0] = 0;
             }         
        }
        fclose(fptr);
     }   
}
/* ******************************************************************************************************************************************* */
void callForwardExternal()
{

     FILE *fptr=fopen(theFile,"w");
     if(fptr)
     {
        fprintf(fptr,"<include>\n");
        fprintf(fptr,"	 <extension name=\"%s:ext %s\">\n",ext.clid,ext.clid);
        fprintf(fptr," 	    <condition field=\"destination_number\" expression=\"^(%s)$\"/>\n",ext.clid);                
        fprintf(fptr," 	    <condition field=\"destination_number\" expression=\"^(%s)$\" require-nested=\"false\">\n",ext.clid);
        fprintf(fptr,"		   <action application=\"set\" data=\"dialed_extension=%s\"/>\n",ext.clid);
        fprintf(fptr,"		   <action application=\"export\" data=\"dialed_extension=%s\"/>\n",ext.clid);
        fprintf(fptr,"		   <action application=\"set\" data=\"transfer_ringback=$${us-ring}\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"cellPhone=%s\"/>\n",ext.cellPhone);
        fprintf(fptr,"		   <action application=\"set\" data=\"call_timeout=%s\"/>\n",ext.CallTimeout);
        fprintf(fptr,"		   <action application=\"set\" data=\"continue_on_fail=true\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"hangup_after_bridge=true\"/>\n");
        
        fprintf(fptr,"		   <action application=\"set\" data=\"vm_zero_xfer=%s\"/>\n",ext.voicemailEscape);
        fprintf(fptr,"		   <action application=\"set\" data=\"vm_zero_domain=%s\"/>\n",ext.domain);
        
        fprintf(fptr,"		   <action application=\"set\" data=\"called_party_callgroup=${user_data(${dialed_extension}@%s var callgroup)}\"/>\n",ext.domain);
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-call_return/${dialed_extension}/${caller_id_number}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/${dialed_extension}/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/${called_party_callgroup}/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/global/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial/${called_party_callgroup}/${uuid}\"/>\n");        
        
        fprintf(fptr,"	       <action application=\"set\" data=\"toll_allow=%s\"/>\n",ext.dialPlan);
        if(strlen(ext.dndCfwdBypass))
        {
            fprintf(fptr,"	       <condition field=\"caller_id_number\" expression=\"^(%s)$\" break=\"on-true\">\n",ext.dndCfwdBypass);
            fprintf(fptr,"	               <action application=\"bridge\" data=\"{sip_invite_domain=%s,presence_id=%s@%s}user/%s@%s\"/>\n",ext.domain,ext.clid,ext.domain,ext.clid,ext.domain);
            fprintf(fptr,"	       		   <action application=\"answer\"/>\n");
            fprintf(fptr,"	       		   <action application=\"sleep\" data=\"1000\"/>\n");
            fprintf(fptr,"	       			<action application=\"voicemail\" data=\"default %s 22\"/>\n",ext.domain);
            fprintf(fptr,"	       </condition>\n");
        }


        fprintf(fptr,"	       <action application=\"transfer\" data=\"%s XML %s\"/>\n",ext.digitsDialed,ext.domain);
        fprintf(fptr,"		   <action application=\"answer\"/>\n");
        fprintf(fptr,"	       <action application=\"sleep\" data=\"1000\"/>\n");
        fprintf(fptr,"	       <action application=\"voicemail\" data=\"default %s %s\"/>\n",ext.domain,ext.clid);
        fprintf(fptr,"	    </condition>\n");
        fprintf(fptr,"   </extension>\n");
        fprintf(fptr,"</include>\n");
        fclose(fptr);
     }
}
/* ******************************************************************************************************************************************* */
void callForwardInternal()
{

     FILE *fptr=fopen(theFile,"w");
     if(fptr)
     {
        xfptr = fopen("/tmp/feature.xml","a+");
        if(xfptr)
        {
                fprintf(xfptr,"opened file %s \n",theFile); 
                fclose(xfptr);
        }

        fprintf(fptr,"<include>\n");
        fprintf(fptr,"	 <extension name=\"%s:ext %s\">\n",ext.clid,ext.clid);
        fprintf(fptr," 	    <condition field=\"destination_number\" expression=\"^(%s)$\"/>\n",ext.clid);                
        fprintf(fptr," 	    <condition field=\"destination_number\" expression=\"^(%s)$\" require-nested=\"false\">\n",ext.clid);
        fprintf(fptr,"		   <action application=\"set\" data=\"dialed_extension=%s\"/>\n",ext.clid);
        fprintf(fptr,"		   <action application=\"export\" data=\"dialed_extension=%s\"/>\n",ext.clid);
        fprintf(fptr,"		   <action application=\"set\" data=\"transfer_ringback=$${us-ring}\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"cellPhone=%s\"/>\n",ext.cellPhone);
        fprintf(fptr,"		   <action application=\"set\" data=\"call_timeout=%s\"/>\n",ext.CallTimeout);
        fprintf(fptr,"		   <action application=\"set\" data=\"continue_on_fail=true\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"hangup_after_bridge=true\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"vm_zero_xfer=%s\"/>\n",ext.voicemailEscape);
        fprintf(fptr,"		   <action application=\"set\" data=\"vm_zero_domain=%s\"/>\n",ext.domain);
        
        fprintf(fptr,"		   <action application=\"set\" data=\"called_party_callgroup=${user_data(${dialed_extension}@%s var callgroup)}\"/>\n",ext.domain);
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-call_return/${dialed_extension}/${caller_id_number}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/${dialed_extension}/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/${called_party_callgroup}/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/global/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial/${called_party_callgroup}/${uuid}\"/>\n");   
        fprintf(fptr,"	       <action application=\"set\" data=\"toll_allow=%s\"/>\n",ext.dialPlan);
        if(strlen(ext.dndCfwdBypass))
        {
            fprintf(fptr,"	       <condition field=\"caller_id_number\" expression=\"^(%s)$\" break=\"on-true\">\n",ext.dndCfwdBypass);
            fprintf(fptr,"	               <action application=\"bridge\" data=\"{sip_invite_domain=%s,presence_id=%s@%s}user/%s@%s\"/>\n",ext.domain,ext.clid,ext.domain,ext.clid,ext.domain);
            fprintf(fptr,"	       		   <action application=\"answer\"/>\n");
            fprintf(fptr,"	       		   <action application=\"sleep\" data=\"1000\"/>\n");
            fprintf(fptr,"	       			<action application=\"voicemail\" data=\"default %s 22\"/>\n",ext.domain);
            fprintf(fptr,"	       </condition>\n");                                                                               
        }
        
        fprintf(fptr,"	       <action application=\"transfer\" data=\"%s XML %s\"/>\n",ext.digitsDialed,ext.domain);
        fprintf(fptr,"		   <action application=\"answer\"/>\n");
        fprintf(fptr,"	       <action application=\"sleep\" data=\"1000\"/>\n");
        fprintf(fptr,"	       <action application=\"voicemail\" data=\"default %s %s\"/>\n",ext.domain,ext.clid);
        fprintf(fptr,"	    </condition>\n");
        fprintf(fptr,"   </extension>\n");
        fprintf(fptr,"</include>\n");
        xfptr = fopen("/tmp/feature.xml","a+");
        if(xfptr)
        {
                fprintf(xfptr,"and wrote parameters, I think %s \n",theFile); 
                fclose(xfptr);
        }
        
        fclose(fptr);
     }
}
/* ******************************************************************************************************************************************* */
void cancelCallForwarding()
{

     FILE *fptr=fopen(theFile,"w");
     if(fptr)
     {

        fprintf(fptr,"<include>\n");
        fprintf(fptr,"	 <extension name=\"%s:ext %s\">\n",ext.clid,ext.clid);
        fprintf(fptr," 	    <condition field=\"destination_number\" expression=\"^(%s)$\"/>\n",ext.clid);                
        fprintf(fptr," 	    <condition field=\"destination_number\" expression=\"^(%s)$\" require-nested=\"false\">\n",ext.clid);
        fprintf(fptr,"		   <action application=\"set\" data=\"dialed_extension=%s\"/>\n",ext.clid);
        fprintf(fptr,"		   <action application=\"export\" data=\"dialed_extension=%s\"/>\n",ext.clid);
        fprintf(fptr,"		   <action application=\"set\" data=\"transfer_ringback=$${us-ring}\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"cellPhone=%s\"/>\n",ext.cellPhone);
        fprintf(fptr,"		   <action application=\"set\" data=\"call_timeout=%s\"/>\n",ext.CallTimeout);
        fprintf(fptr,"		   <action application=\"set\" data=\"continue_on_fail=true\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"hangup_after_bridge=true\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"vm_zero_xfer=%s\"/>\n",ext.voicemailEscape);
        fprintf(fptr,"		   <action application=\"set\" data=\"vm_zero_domain=%s\"/>\n",ext.domain);
        
        fprintf(fptr,"		   <action application=\"set\" data=\"called_party_callgroup=${user_data(${dialed_extension}@%s var callgroup)}\"/>\n",ext.domain);
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-call_return/${dialed_extension}/${caller_id_number}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/${dialed_extension}/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/${called_party_callgroup}/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/global/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial/${called_party_callgroup}/${uuid}\"/>\n");   
        fprintf(fptr,"	       <action application=\"set\" data=\"toll_allow=%s\"/>\n",ext.dialPlan);
        if(strlen(ext.dndCfwdBypass))
        {
            fprintf(fptr,"	       <condition field=\"caller_id_number\" expression=\"^(%s)$\" break=\"on-true\">\n",ext.dndCfwdBypass);
            fprintf(fptr,"	               <action application=\"bridge\" data=\"{sip_invite_domain=%s,presence_id=%s@%s}user/%s@%s\"/>\n",ext.domain,ext.clid,ext.domain,ext.clid,ext.domain);
            fprintf(fptr,"	       		   <action application=\"answer\"/>\n");
            fprintf(fptr,"	       		   <action application=\"sleep\" data=\"1000\"/>\n");
            fprintf(fptr,"	       			<action application=\"voicemail\" data=\"default %s 22\"/>\n",ext.domain);
            fprintf(fptr,"	       </condition>\n");                                                                               
        }
        
        fprintf(fptr,"		   <action application=\"bridge\" data=\"{sip_invite_domain=%s,presence_id=%s@%s}user/%s@%s\"/>\n",ext.domain,ext.clid,ext.domain,ext.clid,ext.domain);
        fprintf(fptr,"		   <action application=\"answer\"/>\n");
        fprintf(fptr,"	       <action application=\"sleep\" data=\"1000\"/>\n");
        fprintf(fptr,"	       <action application=\"voicemail\" data=\"default %s %s\"/>\n",ext.domain,ext.clid);
        fprintf(fptr,"	    </condition>\n");
        fprintf(fptr,"   </extension>\n");
        fprintf(fptr,"</include>\n");
        fclose(fptr);
     }
}
/* ******************************************************************************************************************************************* */
/* ******************************************************************************************************************************************* */
void cancelDND()
{

     char  regFile[2048];
     char  theData[128];
     FILE *fptr=fopen(theFile,"w");
     if(fptr)
     {

        fprintf(fptr,"<include>\n");
        fprintf(fptr,"	 <extension name=\"%s:ext %s\">\n",ext.clid,ext.clid);
        fprintf(fptr," 	    <condition field=\"destination_number\" expression=\"^(%s)$\"/>\n",ext.clid);                
        fprintf(fptr," 	    <condition field=\"destination_number\" expression=\"^(%s)$\" require-nested=\"false\">\n",ext.clid);
        fprintf(fptr,"		   <action application=\"set\" data=\"dialed_extension=%s\"/>\n",ext.clid);
        fprintf(fptr,"		   <action application=\"export\" data=\"dialed_extension=%s\"/>\n",ext.clid);
        fprintf(fptr,"		   <action application=\"set\" data=\"transfer_ringback=$${us-ring}\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"cellPhone=%s\"/>\n",ext.cellPhone);
        fprintf(fptr,"		   <action application=\"set\" data=\"call_timeout=%s\"/>\n",ext.CallTimeout);
        fprintf(fptr,"		   <action application=\"set\" data=\"continue_on_fail=true\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"hangup_after_bridge=true\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"vm_zero_xfer=%s\"/>\n",ext.voicemailEscape);
        fprintf(fptr,"		   <action application=\"set\" data=\"vm_zero_domain=%s\"/>\n",ext.domain);
        
        fprintf(fptr,"		   <action application=\"set\" data=\"called_party_callgroup=${user_data(${dialed_extension}@%s var callgroup)}\"/>\n",ext.domain);
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-call_return/${dialed_extension}/${caller_id_number}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/${dialed_extension}/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/${called_party_callgroup}/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/global/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial/${called_party_callgroup}/${uuid}\"/>\n");   
        fprintf(fptr,"	       <action application=\"set\" data=\"toll_allow=%s\"/>\n",ext.dialPlan);
        if(strlen(ext.dndCfwdBypass))
        {
            fprintf(fptr,"	       <condition field=\"caller_id_number\" expression=\"^(%s)$\" break=\"on-true\">\n",ext.dndCfwdBypass);
            fprintf(fptr,"	               <action application=\"bridge\" data=\"{sip_invite_domain=%s,presence_id=%s@%s}user/%s@%s\"/>\n",ext.domain,ext.clid,ext.domain,ext.clid,ext.domain);
            fprintf(fptr,"	       		   <action application=\"answer\"/>\n");
            fprintf(fptr,"	       		   <action application=\"sleep\" data=\"1000\"/>\n");
            fprintf(fptr,"	       			<action application=\"voicemail\" data=\"default %s 22\"/>\n",ext.domain);
            fprintf(fptr,"	       </condition>\n");
        }
        
        fprintf(fptr,"		   <action application=\"bridge\" data=\"{sip_invite_domain=%s,presence_id=%s@%s}user/%s@%s\"/>\n",ext.domain,ext.clid,ext.domain,ext.clid,ext.domain);
        fprintf(fptr,"		   <action application=\"answer\"/>\n");
        fprintf(fptr,"	       <action application=\"sleep\" data=\"1000\"/>\n");
        fprintf(fptr,"	       <action application=\"voicemail\" data=\"default %s %s\"/>\n",ext.domain,ext.clid);
        fprintf(fptr,"	    </condition>\n");
        fprintf(fptr,"   </extension>\n");
        fprintf(fptr,"</include>\n");
        fclose(fptr);
     }
     fptr = fopen(theDndFile,"r");
     if(fptr)
     {
         while(!feof(fptr))
         {
             fgets(theData,127,fptr);
             if(strstr(theData,"DNDStatus"))
                strcat(regFile,"			<variable name=\"DNDStatus\" value=\"OFF\"/>\n");
             else if(strstr(theData,"<include>"))
                strcpy(regFile,theData);
             else if(strstr(theData,"</include>"))
             {
                  strcat(regFile,theData);
                  fclose(fptr);
                  break;
             }
             else
                strcat(regFile,theData);                           
         }   
     }
     fptr = fopen(theDndFile,"w");
     if(fptr)
     {
         fprintf(fptr,"%s",regFile);
         fclose(fptr);
     }
     
}
/* ******************************************************************************************************************************************* */
void enableDND()
{

     char  regFile[2048];
     char  theData[128];
     
     FILE *fptr=fopen(theFile,"w");
     if(fptr)
     {

        fprintf(fptr,"<include>\n");
        fprintf(fptr,"	 <extension name=\"%s:ext %s\">\n",ext.clid,ext.clid);
        fprintf(fptr," 	    <condition field=\"destination_number\" expression=\"^(%s)$\"/>\n",ext.clid);        
        fprintf(fptr," 	    <condition field=\"destination_number\" expression=\"^(%s)$\" require-nested=\"false\">\n",ext.clid);
        fprintf(fptr,"		   <action application=\"set\" data=\"dialed_extension=%s\"/>\n",ext.clid);
        fprintf(fptr,"		   <action application=\"export\" data=\"dialed_extension=%s\"/>\n",ext.clid);
        fprintf(fptr,"		   <action application=\"set\" data=\"transfer_ringback=$${us-ring}\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"cellPhone=%s\"/>\n",ext.cellPhone);
        fprintf(fptr,"		   <action application=\"set\" data=\"call_timeout=%s\"/>\n",ext.CallTimeout);
        fprintf(fptr,"		   <action application=\"set\" data=\"continue_on_fail=true\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"hangup_after_bridge=true\"/>\n");
        fprintf(fptr,"		   <action application=\"set\" data=\"vm_zero_xfer=%s\"/>\n",ext.voicemailEscape);
        fprintf(fptr,"		   <action application=\"set\" data=\"vm_zero_domain=%s\"/>\n",ext.domain);
        
        fprintf(fptr,"		   <action application=\"set\" data=\"called_party_callgroup=${user_data(${dialed_extension}@%s var callgroup)}\"/>\n",ext.domain);
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-call_return/${dialed_extension}/${caller_id_number}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/${dialed_extension}/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/${called_party_callgroup}/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial_ext/global/${uuid}\"/>\n");
        fprintf(fptr,"		   <action application=\"hash\" data=\"insert/group1.sipline.ca-last_dial/${called_party_callgroup}/${uuid}\"/>\n");   
        fprintf(fptr,"	       <action application=\"set\" data=\"toll_allow=%s\"/>\n",ext.dialPlan);
        if(strlen(ext.dndCfwdBypass))
        {
            fprintf(fptr,"	       <condition field=\"caller_id_number\" expression=\"^(%s)$\" break=\"on-true\">\n",ext.dndCfwdBypass);
            fprintf(fptr,"	               <action application=\"bridge\" data=\"{sip_invite_domain=%s,presence_id=%s@%s}user/%s@%s\"/>\n",ext.domain,ext.clid,ext.domain,ext.clid,ext.domain);
            fprintf(fptr,"	       		   <action application=\"answer\"/>\n");
            fprintf(fptr,"	       		   <action application=\"sleep\" data=\"1000\"/>\n");
            fprintf(fptr,"	       			<action application=\"voicemail\" data=\"default %s 22\"/>\n",ext.domain);
            fprintf(fptr,"	       </condition>\n");
        }
        
        fprintf(fptr,"	       <action application=\"transfer\" data=\"8%s XML %s\"/>\n",ext.clid,ext.domain);
        fprintf(fptr,"		   <action application=\"answer\"/>\n");
        fprintf(fptr,"	       <action application=\"sleep\" data=\"1000\"/>\n");
        fprintf(fptr,"	       <action application=\"voicemail\" data=\"default %s %s\"/>\n",ext.domain,ext.clid);
        fprintf(fptr,"	    </condition>\n");
        fprintf(fptr,"   </extension>\n");
        fprintf(fptr,"</include>\n");
        fclose(fptr);
     }
     fptr = fopen(theDndFile,"r");
     if(fptr)
     {
         while(!feof(fptr))
         {
             fgets(theData,127,fptr);
             if(strstr(theData,"DNDStatus"))
                strcat(regFile,"			<variable name=\"DNDStatus\" value=\"ON\"/>\n");
             else if(strstr(theData,"<include>"))
                strcpy(regFile,theData);
             else if(strstr(theData,"</include>"))
             {
                  strcat(regFile,theData);
                  fclose(fptr);
                  break;
             }
             else
                strcat(regFile,theData);                           
         }   
     }
     fptr = fopen(theDndFile,"w");
     if(fptr)
     {
         fprintf(fptr,"%s",regFile);
         fclose(fptr);
     }
     
}
/* ******************************************************************************************************************************************* */
/* ********************************************************************************************************************************* */
int	updateXmlFiles()//xmlUpdateReturn
{
	FILE *fptr;
	char	UpdateFreeSwitch[128];
	char    xmlUpdateReturn[256];
	sprintf(UpdateFreeSwitch,"/./fs -p \"%s\" -x \"reloadxml\"",fscliPassword);	
    fptr = popen(UpdateFreeSwitch, "r");
	if(fptr)
	{
		fread(xmlUpdateReturn,1,127,fptr);		
		pclose(fptr);
	}
	//+OK [Success]

			return(0);

}
/* ******************************************************************************************************************************************* */
