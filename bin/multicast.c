/*
multicast.c

The following program sends or receives multicast packets. If invoked
with one argument, it sends a packet containing the current time to an
arbitrarily chosen multicast group and UDP port. If invoked with no
arguments, it receives and prints these packets. Start it as a sender on
just one host and as a receiver on all the other hosts

*/
#include <ctype.h>
#include "/usr/include/mariadb/mysql.h"
#include <stdio.h>
#include <stdlib.h>
#include  <string.h>
#include <dirent.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <stdio.h>

#include <string.h>
#include <stdlib.h>
#define EXAMPLE_PORT 5060
#define EXAMPLE_GROUP "224.0.1.75"
void main(int argc)
{
   struct sockaddr_in addr;
   int addrlen, sock, cnt;
   struct ip_mreq mreq;
   char message[750];
   char sendMessage[750];
   char subscribe[128];
   char via[128];
   char from[128];
   char to[128];
   char callid[128];
   char useragent[128];
   char cseq[128];
   char expires[128];
   char contentlength[128];
   char maxforwards[128];
   char subscriptionstate[128];
   char event[128];
   char contenttype[128];
   char messagebody[128];
   char notify[128];
   char *adest,*bdest,*cdest;
   char * test;
   char	macaddress[15];
   char myPort[24];
   char myCommand[128];
   char	myIpAddress[16];   
   char	nwData[256];   
   sprintf(myCommand,"nmcli device status");
   
	FILE *nwPtr = popen(myCommand,"r");
  if(nwPtr)
  {
  	while(!feof(nwPtr))
	{	  
		fgets(nwData,255,nwPtr);
		if(strstr(nwData,"ethernet"))
		{
			adest = strchr(nwData,' ');
			nwData[adest-nwData]=0;		
			break;
		}
	}
	pclose(nwPtr);
  }
   
   sprintf(myCommand,"ifconfig %s:0",nwData);
   system(myCommand);
   sprintf(myCommand,"ifconfig %s:0 192.168.199.199",nwData);
   system(myCommand);
   
   
   int i;
   /* set up socket */
   sock = socket(AF_INET, SOCK_DGRAM, 0);
   if (sock < 0) {
     perror("socket");
     exit(1);
   }
   bzero((char *)&addr, sizeof(addr));
   addr.sin_family = AF_INET;
   addr.sin_addr.s_addr = inet_addr(EXAMPLE_GROUP);
   addr.sin_port = htons(EXAMPLE_PORT);
   addrlen = sizeof(addr);   
   
	sprintf(myCommand,"hostname -I | awk '{print $1}'");   
	FILE *ptr = popen(myCommand,"r");
	if(ptr)
	{
		fgets(myIpAddress,24,ptr);
		pclose(ptr);
	} 
	myIpAddress[strlen(myIpAddress)-1]=0;

	if(strlen(myIpAddress)< 5)
	{
		sprintf(myCommand,"hostname -I | awk '{print $2}'");   
		FILE *ptr = popen(myCommand,"r");
		if(ptr)
		{
			fgets(myIpAddress,24,ptr);
			pclose(ptr);
		} 	
		myIpAddress[strlen(myIpAddress)-1]=0;
	}	
	FILE *myPtr = fopen("/tmp/test.xml","a");
			if(myPtr)
			{
				fprintf(myPtr,"ip is [%s] interface [%s]\n",myIpAddress,nwData);
				fclose(myPtr);
			}   	
	  /* receive */
	if (bind(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) 
	{
		perror("bind");
		exit(1);
	}    
	mreq.imr_multiaddr.s_addr = inet_addr(EXAMPLE_GROUP);         
	//mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	mreq.imr_interface.s_addr = inet_addr("192.168.199.199");
	if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP,
	 &mreq, sizeof(mreq)) < 0) 
	{
		 perror("setsockopt mreq");
		 exit(1);
	}         
	while (1) 
	{
		cnt = recvfrom(sock, message, sizeof(message), 0, (struct sockaddr *) &addr, &addrlen);	
		printf("%s:%d receving message = \"%s\" size <%d>\n", inet_ntoa(addr.sin_addr),ntohs(addr.sin_port), message,sizeof(message));
		//i=0; i < sizeof(message);++i)
		//	printf("%c - %d |",message[i],message[i]);
		// to parse message into lines, end of line is marked with LF ascii 10	
		
		adest = strstr(message,"SUBSCRIBE");				
		if(adest)	
		{			
			bdest = strchr(&message[adest-message],10);
			strncpy(subscribe,&message[adest-message],bdest-adest+1);
			subscribe[bdest-adest+1]=0;
		}		
		
		adest = strstr(message,"Via:");				
		if(adest)	
		{			
			bdest = strchr(&message[adest-message],10);
			strncpy(via,&message[adest-message],bdest-adest+1);
			via[bdest-adest+1]=0;
		}		
		
		adest = strstr(message,"From:");		
		if(adest)	
		{			
			bdest = strchr(&message[adest-message],10);
			strncpy(from,&message[adest-message],bdest-adest+1);
			from[bdest-adest+1]=0;

		} 
		
		adest = strstr(message,"User-Agent:");		
		if(adest)	
		{			
			bdest = strchr(&message[adest-message],10);
			strncpy(useragent,&message[adest-message],bdest-adest+1);
			useragent[bdest-adest+1]=0;

		}
		
		adest = strstr(message,"To:");		
		if(adest)	
		{			
			bdest = strchr(&message[adest-message],10);
			strncpy(to,&message[adest-message],bdest-adest+1);
			to[bdest-adest+1]=0;			
		}		

		adest = strstr(message,"Call-ID:");		
		if(adest)	
		{			
			bdest = strchr(&message[adest-message],10);
			strncpy(callid,&message[adest-message],bdest-adest+1);
			callid[bdest-adest+1]=0;
		}
		
		adest = strstr(message,"CSeq:");		
		if(adest)	
		{			
			bdest = strchr(&message[adest-message],10);
			strncpy(cseq,&message[adest-message],bdest-adest+1);
			cseq[bdest-adest+1]=0;
		}
		
		
		sprintf(expires,"Expires: 0\n");
		sprintf(contentlength,"Content-Length: 0\n");
		
		sprintf(sendMessage,"SIP/2.0 200 OK\n%s%s%s%s%s%s%s",via,from,to,callid,cseq,expires,contentlength);
		
		
		if(cnt > 0)		
		{			 
			printf("\n\nsending: \n%s\n", sendMessage);
			sendto(sock, sendMessage, strlen(sendMessage), 0,(struct sockaddr *) &addr, addrlen);
			cnt = 0;
			adest=strstr(subscribe,"MAC");
			if(adest)
			{
				bdest = strchr(subscribe,'@');
				strncpy(macaddress,&subscribe[adest-subscribe+3],bdest-adest-3);
				macaddress[bdest-adest-3]=0;
				if(strchr(macaddress,'%'))
					sprintf(macaddress,"%s",&macaddress[3]);
				
			}
			

			 sprintf(myCommand,"hostname -I | awk '{print $2}'");
			  FILE *ptr = popen(myCommand,"r");
			  if(ptr)
			  {
				fgets(myIpAddress,24,ptr);
				pclose(ptr);
			  }
			  myIpAddress[strlen(myIpAddress)-1]=0;

			sprintf(notify,"NOTIFY sip:MAC:%s@%s:%d SIP/2.0\r\n",macaddress,inet_ntoa(addr.sin_addr),ntohs(addr.sin_port));
			sprintf(cseq,"Cseq: 3 NOTIFY\r\n");
			sprintf(maxforwards,"Max-Forwards: 70\r\n");
			sprintf(subscriptionstate,"Subscription-State: terminated;reason=noresource\r\n");
			sprintf(event,"Event: ua-profile\r\n");
			sprintf(contenttype,"Content-Type: application/url\r\n");
			sprintf(messagebody,"tftp://%s/%s.cfg",myIpAddress,macaddress);
			
			myPtr = fopen("/tmp/test.xml","a");
			if(myPtr)
			{
				fprintf(myPtr,"messagebody is [%s]\n",messagebody);
				fprintf(myPtr,"notify at [%s]\n",notify);
				fclose(myPtr);
			}  

			sprintf(contentlength,"Content-Length: %d\r\n",strlen(messagebody));
			sprintf(sendMessage,"%s%s%s%s%s%s%s%s%s%s%s\r\n%s\r\n",notify,via,from,to,callid,cseq,maxforwards,subscriptionstate,event,contenttype,contentlength,messagebody);
			sendto(sock, sendMessage, strlen(sendMessage), 0,(struct sockaddr *) &addr, addrlen);
			printf("SEND NOTIFY\n%s",sendMessage);
		}					
	}
}
