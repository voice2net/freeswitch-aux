wget http://provision.voipsystems.ca/repo/packages.txt

echo "deb http://provision.voipsystems.ca/repo/deb/freeswitch-1.8/ stretch main" > /etc/apt/sources.list.d/freeswitch.list
echo "deb-src http://provision.voipsystems.ca/repo/deb/freeswitch-1.8/ stretch main" >> /etc/apt/sources.list.d/freeswitch.list

apt-get update

cat packages.txt | xargs apt-get --allow-unauthenticated -y  install

cd /usr/src

git clone https://voice2net@bitbucket.org/voice2net/freeswitch.git

cd /usr/src/freeswitch

./bootstrap.sh -j
./configure
make
make install

cd /usr/local
groupadd freeswitch
adduser --quiet --system --home /usr/local/freeswitch --gecos "FreeSWITCH open source softswitch" --ingroup freeswitch freeswitch --disabled-password
chown -R freeswitch:freeswitch /usr/local/freeswitch/
chmod -R ug=rwX,o= /usr/local/freeswitch/
chmod -R u=rwx,g=rx /usr/local/freeswitch/bin/*

ln -s /usr/local/freeswitch/bin/fs_cli /fs

wget http://provision.voipsystems.ca/repo/freeswitch.service

cp freeswitch.service /etc/systemd/system/freeswitch.service

systemctl start freeswitch

systemctl enable freeswitch

apt-get install apache2 -y
apt-get install php -y
apt-get install php-mysql -y
apt-get install php-dev -y
apt-get install mysql-server -y

rm /etc/localtime
ln -s /usr/share/zoneinfo/Canada/Eastern /etc/localtime

apt -y install fail2ban

systemctl start fail2ban
systemctl start mysqld
mysql_secure_installtion
rm /etc/localtime

ln -s /usr/share/zoneinfo/Canada/Eastern /etc/localtime

tar xvC /usr/local/freeswitch -f /usr/src/freeswitch/sounds/sounds.tar
