<?php
 //if registrations found, returns "Active Registrations", else returns "no Registrations"
 $password = "pbx2net"; 
 $port = "8021";
 $host = "192.168.26.51";

 function event_socket_create($host, $port, $password) 
 {

     $fp = fsockopen($host, $port, $errno, $errdesc) 
       or die("Connection to $host failed");
     socket_set_blocking($fp,false);
		
     if ($fp) 
	 {
         while (!feof($fp)) 
		 {
            $buffer = fgets($fp, 1024);
            usleep(100); //allow time for reponse
            if (trim($buffer) == "Content-Type: auth/request") {
               fputs($fp, "auth $password\n\n");
               break;
            }
         }
         return $fp;
     }
     else 
	 {
         return false;
     }           
 }
 
 function event_socket_request($fp, $cmd) 
 {
    
     if ($fp) 
	 {    
		
         fputs($fp, $cmd."\n\n");    
         usleep(100); //allow time for reponse
         
         $response = "";
         $i = 0;
         $contentlength = 0;
		         while (!feof($fp)) 
		 {

            $buffer = fgets($fp, 4096);

            if ($contentlength > 0) 
			{
               $response .= $buffer;
            }
            
            if ($contentlength == 0) { //if contentlenght is already don't process again
                if (strlen(trim($buffer)) > 0) 
				{ //run only if buffer has content
                    $temparray = split(":", trim($buffer));
                    if ($temparray[0] == "Content-Length") 
					{
                       $contentlength = trim($temparray[1]);
                    }
                }
            }
            
            usleep(100); //allow time for reponse
            
            //optional because of script timeout //don't let while loop become endless
            if ($i > 10) { break; } 
            
            if ($contentlength > 0) 
			{ //is contentlength set
                //stop reading if all content has been read.
                if (strlen($response) >= $contentlength) 
				{  
                   break;
                }
            }
            $i++;
         }
         
         return $response;
     }
     else {
       
     }
 }
 
 $file = fopen("/usr/local/freeswitch/conf/directory/$argv[2]/$argv[3].xml","r");
 $theResult=fread($file,"512"); 
 pclose($file);
 if (strpos($theResult, 'OFF') !== false) 
 {
	 
	 exec("sed -i 's/.*variable name=.*/           <variable name=\"$argv[3]\" value=\"ON\"\/>/' /usr/local/freeswitch/conf/directory/$argv[2]/$argv[3].xml");
	 exec("sed -i 's/.*are_we_open=.*/           <action application=\"set\" data=\"are_we_open=No\" inline=\"true\"\/>/' /usr/local/freeswitch/conf/dialplan/$argv[2]/$argv[4].xml");
	 $data[7]="answer-state: confirmed\n";
	 $niteStatus="ON";
 }
 else
 {
	 
    exec("sed -i 's/.*variable name=.*/           <variable name=\"$argv[3]\" value=\"OFF\"\/>/' /usr/local/freeswitch/conf/directory/$argv[2]/$argv[3].xml");
	exec("sed -i 's/.*are_we_open=.*/           <action application=\"set\" data=\"are_we_open=Yes\" inline=\"true\"\/>/' /usr/local/freeswitch/conf/dialplan/$argv[2]/$argv[4].xml");
    $data[7]="answer-state: terminated\n";	
	$niteStatus="OFF";
 }
 
  
 
 
$data[0]="sendevent PRESENCE_IN\n";
$data[1]="proto: sip\n";
$data[2]="from: $argv[1]\n";
$data[3]="login: $argv[1]\n";
$data[4]="event_type: presence\n";
$data[5]="alt_event_type: dialog\n";
$data[6]="Presence-Call-Direction: inbound\n";
$data[8]="unique-id: foo\n";
 $sendthis="$data[0]$data[1]$data[2]$data[3]$data[4]$data[5]$data[6]$data[7]$data[8]";
 $fp = event_socket_create($host, $port, $password);
 //echo "eventsocket <$fp>".PHP_EOL; 
  $response = event_socket_request($fp, $sendthis);
 //echo $sendthis; 
 //echo "the response $response is good"; 
 fclose($fp);   
 echo $niteStatus;  
 ?>

