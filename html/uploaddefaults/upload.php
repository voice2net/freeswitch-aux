<?php

$target_dir = "/usr/local/freeswitch/wavfiles/newdomain/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$source_file= basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

	if ($_FILES["fileToUpload"]["size"] > 10000000) 
	{
		echo "File is too large.";
		$uploadOk = 0;
	}
	if($imageFileType != "wav") 
	{
		echo "Only .wav files are allowed.";
		$uploadOk = 0;
	}
	if ($uploadOk == 0) 
	{
		echo "The file was not uploaded.";
	}
	else 
	{    
		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) 
		{
			echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		} 
		else 
		{
			echo "Sorry, not uploaded"  ;
		}
	}
?>